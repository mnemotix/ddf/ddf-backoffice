/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


const path = require('path');
const webpack = require('webpack');
const autoprefixerStylus = require('autoprefixer-stylus');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const fs = require('fs');


const browserslist = fs.readFileSync(path.resolve(__dirname, '.browserslist')).toString().split("\n");

let isDev = process.env.NODE_ENV !== 'production';
let isProd = !isDev;
let isCI = !!process.env.CI;

/** mode **/
let mode = isDev ? 'development' : 'production';

/** devtool **/
let devtool = isDev ? 'eval-source-map' : 'nosources-source-map';

/** entry **/
let entry = [
  '@babel/polyfill',
  ...(isDev ? ['webpack-hot-middleware/client?reload=true'] : []), 
  path.resolve(__dirname, './src/client/main.js')
];

/** output **/
let output = {
  path: path.resolve(__dirname, 'dist'),
  filename: isDev ? '[name].js' : '[name].[hash].js',
  chunkFilename: isDev ? '[name].js' : '[name].[hash].js',
  publicPath: '/'
};

/** plugins **/
let plugins = [
  ...(!isCI ? [new webpack.ProgressPlugin()] : []),
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, './src/client/index.tpl.html'),
    inject: 'body',
    filename: 'index.html'
  }),
   ...(isDev ? [new webpack.HotModuleReplacementPlugin()] : []), 
  ...(isProd ? [new MiniCssExtractPlugin({
    filename: '[name].[hash].css',
    chunkFilename: '[id].[hash].css',
  })] : []),
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.DefinePlugin({
    'NODE_ENV': process.env.NODE_ENV
  })
];

/** optimization **/
let optimization = {};
if (isProd) {
  optimization.minimizer = [
    new TerserPlugin({
      extractComments: true,
      sourceMap: true
    }),
    new OptimizeCSSAssetsPlugin({})
  ]
}

/** modules **/
let webpackModule = {
  rules: [
    {
      test: /\.m?js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          rootMode: 'upward'
        }
      }]
    },
    {
      /* For some libraries using ES6 modules (ex: react-relay-network-modern), this is needed to ensure
       * correct interpretation of .mjs files as javascript */
      test: /\.mjs$/,
      include: /node_modules/,
      type: "javascript/auto",
    },
    {
      test: /\.styl/,
      exclude: [/node_modules/, /cortex/],
      use: [{
        loader: isDev ? 'style-loader' : MiniCssExtractPlugin.loader
      }, {
        loader: 'css-loader',
        options: {
          importLoaders: 2,
          modules: true,
          localIdentName: isDev ? '[name]__[local]___[hash:base64:5]' : '[sha1:hash:hex:4]',
          sourceMap: true
        }
      }, {
        loader: 'stylus-loader',
        options: {
          use: [
            autoprefixerStylus()
          ]
        }
      }]
    },
    {
      test: /\.css/,
      include: /node_modules/,
      use: [{
        loader: isDev ? 'style-loader' : MiniCssExtractPlugin.loader
      },{
        loader: 'css-loader'
      }]
    },
    {
      test: /\.(woff|woff2|eot|ttf)$/,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 4096
        }
      }]
    },
    {
      test: /\.(jpe?g|png|gif|svg|ico)$/i,
      use: {
        loader: 'file-loader',
        options: {
          name: '[path][name].[hash].[ext]'
        }
      }
    },
    {
      test: /\.md/,
      use: 'raw-loader'
    }
  ],
};

const webpackConfig = {
  mode,
  devtool,
  entry,
  output,
  plugins,
  optimization,
  module: webpackModule,
  resolve: {
    extensions: [
      '.js', 
      '.json', /* needed because some dependencies use { import './Package' } expecting to resolve Package.json */
      '.mjs',  /* for libraries shipping ES6 module to work */
    ]
  },
  stats: {
    excludeAssets: [/favicon-plugin-icons-/],
    colors: true,
    hash: false,
    timings: true,
    chunks: false,
    chunkModules: false,
    modules: false
  },
};

module.exports = webpackConfig;
