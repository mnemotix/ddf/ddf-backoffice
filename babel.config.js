module.exports =  function(api) {
  let isDev = process.env.NODE_ENV !== 'production';

  api.cache(false);

  const presets = [
    ["@babel/env", {
      modules: "cjs"
    }],
    "@babel/react"
  ];

  const plugins = [
    ["@babel/transform-async-to-generator"],
    ["@babel/proposal-class-properties"],
    ["@babel/proposal-object-rest-spread"],
    ["@babel/proposal-optional-chaining"],
    ["jsx-control-statements"]
  ];

  if(isDev) {
    plugins.push("@babel/plugin-transform-react-jsx-source")
  }


  return {
    presets,
    plugins
  }
};
