import React, {useState} from 'react';
import Dropdown from "react-dropdown";
import {DesktopMainLayout} from "./layouts/desktop/DesktopMainLayout";
import {FormDataSourceLocationInput} from "./components/generic/FormDataSourceLocationInput";
import {BorderedButton} from "./components/generic/widgets/BorderedButton";
import style from "./DDFBackoffice.styl";
import {useMutation, useSubscription} from "react-apollo";
import gql from "graphql-tag";
import 'react-dropdown/style.css'

const datasourceTypes = [{
  label: "Wiktionnaire",
  value: "wiktionnaire",
  defaultDataSourceUrl: "https://dumps.wikimedia.org/frwiktionary/latest/frwiktionary-latest-pages-articles.xml.bz2"
}];

const datasourceLoadTaskProgresses = [];

export const DDFBackoffice = () => {
  const [buttonDisabled, setButtonDisabled] = useState(false);
  const [datasourceURL, setDatasourceURL] = useState(datasourceTypes[0].defaultDataSourceUrl);
  const [datasourceType, setDatasourceType] = useState(datasourceTypes[0]);
  const [datasourceLoadTaskProgresses, setDatasourceLoadTaskProgress] = useState([]);

  const temporarlySnoozeButton = (duration = 5000) => {
    setButtonDisabled(true);
    setTimeout(() => {
      setButtonDisabled(false);
    }, duration);
  };

  const {data} = useSubscription(gql`
    subscription {
      datasourceLoadTaskProgress{
        currentStage
        datasourceType
        datasourceURL
        stageProgress
        totalStages
        stageStatus
      }
    }
  `);

  if(data?.datasourceLoadTaskProgress){
    let indexOf = datasourceLoadTaskProgresses.findIndex(({datasourceURL}) => datasourceURL === data.datasourceLoadTaskProgress.datasourceURL);

    if (indexOf === -1){
      datasourceLoadTaskProgresses.push(data.datasourceLoadTaskProgress);
    } else {
      datasourceLoadTaskProgresses.splice(indexOf, 1, data.datasourceLoadTaskProgress);
    }
  }

  const [createDatasourceLoadTask] = useMutation(gql`
    mutation ($datasourceURL: String!, $datasourceType: DatasourceType!){
      createDatasourceLoadTask(input: {objectInput: {datasourceURL: $datasourceURL, datasourceType: $datasourceType}}){
        createdEdge{
          node{
            datasourceURL
          }
        }
      }
    }
  `);

  let calcultateProgress = ({stageProgress, totalStages, currentStage}) => {
    return ((currentStage - 1) / totalStages * 100) + stageProgress / totalStages;
  };


  return (
    <DesktopMainLayout>
      <div className={style.section}>
        <div className={style.title}>Nouvel import de données</div>
        <Dropdown options={datasourceTypes}
                  onChange={(datasourceType) => setDatasourceType(datasourceType)}
                  value={datasourceType}
                  placeholder="Sélectionner une source de données"
                  className={style.dropdown}
                  controlClassName={style.control}
                  menuClassName={style.menu}
        />

        <FormDataSourceLocationInput placeholder={"URL du fichier de données à charger"}
                                     onChange={(evt) => setDatasourceURL(evt.target.value)}
                                     value={datasourceType?.defaultDataSourceUrl}
        />

        <div className={style.button}>
          <BorderedButton disabled={buttonDisabled} text={"Importer"} onClick={async () => {
            temporarlySnoozeButton();
            await createDatasourceLoadTask({variables: {datasourceURL, datasourceType: datasourceType.value}})
          }}/>
        </div>
      </div>

      <div className={style.section}>
        <div className={style.title}>Liste des imports en cours</div>
      </div>

      <If condition={datasourceLoadTaskProgresses.length > 0}>
        <For each="datasourceLoadTaskProgress" of={datasourceLoadTaskProgresses}>
          <div className={style.progressTitle}>
            {datasourceLoadTaskProgress.datasourceURL}
          </div>
          <div className={style.progress}>
            <div className={style.bar} style={{width: `${calcultateProgress(datasourceLoadTaskProgress)}%`}}/>
            <For index="idx" of={[...Array(datasourceLoadTaskProgress.totalStages).keys()]}>
              <div key={idx} className={style.fragment} style={{
                width: `${1 / datasourceLoadTaskProgress.totalStages * 100}%`,
                left: `${idx / datasourceLoadTaskProgress.totalStages * 100}%`
              }}/>
            </For>
          </div>
          <div className={style.progressDetails}>
            {datasourceLoadTaskProgress.stageStatus} ({datasourceLoadTaskProgress.currentStage}/{datasourceLoadTaskProgress.totalStages})
          </div>
        </For>
      </If>
    </DesktopMainLayout>
  )
};
