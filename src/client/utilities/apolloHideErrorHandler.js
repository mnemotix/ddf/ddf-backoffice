export function apolloHideErrorHandler() {
  let stack = new Error("Apollo error catched").stack;
  return function(error) {
    console.error(stack);
    console.error(error);
    return null;
  }
}
