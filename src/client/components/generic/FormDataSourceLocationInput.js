import React, {useState, useEffect, useRef} from 'react';
import PropTypes from "prop-types";
import classNames from 'classnames';

import style from './FormSearchInput.styl';
import searchIcon from '../../assets/images/exclamation_white.svg';

/**
 *
 * Usage :
 *
 * <FormDataSourceLocationInput
 *  theme='dark'
 *  size='large'
 *  autofocus={false}
 * />
 *
 *
 * Properties : 
 *
 * - theme : 'dark'|'light' (default)
 * - size  : 'large'(default)|'medium' 
 * - autofocus : true (default)|false
 *
 */
export const FormDataSourceLocationInput = ({placeholder, closeButton, onChange, onSubmit, onClose, value, ...props}) => {
  const inputRef = useRef();
  const [input, setInput] = useState(value || "");
  const [theme] = useState(props.theme || 'light');
  const [size] = useState(props.size || 'medium');
  const [textSize, setTextSize] = useState("normal");
  const [autofocus] = useState(!!props.autofocus);

  useEffect(() => {
    if(autofocus) {
      inputRef.current.focus();
    }
  });

  let styles = [
    style.container,
    style[theme],
    style[size],
    style[`text-size-${textSize}`]
  ];

  const handleChange = (evt) => {
    let inputValue = evt.target.value;
    let textSize = inputValue.length > 20 ? 'small' : 'normal';
    setInput(inputValue);
    setTextSize(textSize);

    if(onChange) {
      onChange(evt);
    }
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();
    if(!input) {
      return;
    }
    if(onSubmit) {
      onSubmit(evt);
    }
  };

  const handleOnClose = () => {
    if(onClose) {
      onClose();
    }
  };

  return (
    <div className={classNames(styles)}>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder={placeholder}
          onChange={handleChange}
          value={input}
          ref={inputRef}
        />
      </form>

      <Choose>
        <When condition={closeButton}>
          <div className={style.closeIcon} onClick={handleOnClose}>
            &#x2715;
          </div>
        </When>
        <Otherwise>
          <div className={style.rightHandPlaceholder}/>
        </Otherwise>
      </Choose>

    </div>
  );
};

FormDataSourceLocationInput.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func,
  theme: PropTypes.oneOf(['dark', 'light']),
  size: PropTypes.oneOf(['large', 'medium']),
  closeButton: PropTypes.bool,
  autofocus: PropTypes.bool
};