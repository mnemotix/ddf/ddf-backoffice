import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import style from './BorderedButton.styl';


/**
 *
 * Usage : 
 *
 * <BorderedButton 
 *  text="Rechercher"
 *  onClick={() => this.performSearch()}
 *  className="searchButton"
 *  theme={theme}
 * />
 *
 * `theme` is an object as imported by CSS modules. You may use the CSS classname you provided as `className`, and the nested class `.text`.
 *
 *  CSS module file : 
 *
 *  .searchButton {
 *     ...
 *  }
 *  .searchButton .text {
 *    ...
 *  }
 *
 *
 */
export class BorderedButton extends React.Component {

  static propTypes = {
    className: PropTypes.string,
    theme: PropTypes.object,
    onClick: PropTypes.func,
    text: PropTypes.string,
    disabled: PropTypes.bool
  };

  static defaultProps = {
    className: 'borderedButton',
    theme: {},
    disabled: false
  }

  constructor(props) {
    super(props);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  render() {
    const {theme, disabled} = this.props;
    const customClassName = this.props.className;

    return (
      <button className={classNames([style.button, theme[customClassName]], {[style.disabled]: disabled})} onClick={this.onClickHandler}>
        <div className={classNames([style.text, theme.text])}>{this.props.text}</div>
      </button>
    );
  }

  onClickHandler() {
    if (this.props.onClick) {
      this.props.onClick();
    }
  }
}
