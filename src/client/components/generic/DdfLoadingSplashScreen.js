import React from 'react';

import DdfLogo from '../../assets/images/ddf_logo.png';
import style from './DdfLoadingSplashScreen.styl';

export const DdfLoadingSplashScreen = ({children}) => {
  return (
    <div className={style.screen}>
      <div className={style.logo}>
        <img src={DdfLogo} alt={"Logo"}/>
      </div>
      {children}
    </div>
  );
};
