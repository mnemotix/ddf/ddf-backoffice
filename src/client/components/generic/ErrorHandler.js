import React from 'react';

export class ErrorHandler extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    console.error(`${this.constructor.name} catched an error`);
    console.error(error, info);
  }

  render() {
    throw('Needs to be implented');
  }
}


