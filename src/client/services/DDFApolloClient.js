import {ApolloClient} from "apollo-client";
import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

// Create an http link:
const httpLink = new HttpLink({
  uri: `${location.origin}/graphql`
});

// Create a WebSocket link:
const wsLink = new WebSocketLink({
  uri: `${location.protocol.replace("http", "ws")}//${location.host}/graphql/subscriptions`,
  options: {
    reconnect: true
  }
});

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    );
  },
  wsLink,
  httpLink,
);

import {InMemoryCache} from 'apollo-cache-inmemory';

const cache = new InMemoryCache();

export const DDFApolloClient = new ApolloClient({
  link,
  cache
});

