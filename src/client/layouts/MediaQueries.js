import React from 'react';
import ReactResponsive from 'react-responsive';
import mediaQueries from '../assets/stylesheets/mediaQueries.json';

export const Desktop = props => <ReactResponsive {...props} minWidth={mediaQueries.desktopMinWidth} />;
export const Mobile = props => <ReactResponsive {...props} maxWidth={mediaQueries.mobileMaxWidth} />;

