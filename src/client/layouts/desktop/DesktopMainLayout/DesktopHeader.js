import React from 'react';
import {useTranslation} from "react-i18next";

import ddfLogo from '../../../assets/images/ddf_logo.png';
import style from './DesktopHeader.styl';

export const DesktopHeader = () => {
  const {t} = useTranslation();

  return (
    <div className={style.header}>
      <div className={style.headerSection}>
        <img className={style.ddfLogo} src={ddfLogo}/>
        <div className={style.ddfTitle}>
          {t('DESKTOP_HEADER.TITLE')}
        </div>
      </div>

      <div className={style.headerSection}>
      </div>
    </div>
  );
};
