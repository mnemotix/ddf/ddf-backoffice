import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import {DesktopHeader} from './DesktopMainLayout/DesktopHeader';

import style from './DesktopMainLayout.styl';

/**
 *
 * Usage :
 *
 * <DesktopMainLayout sideColumn={SideColumnContent} />
 *
 * If a side column is given, the layout will be 3/4 for main content and 1/4 for side column content,
 * otherwise the layout is a single centered column
 */
export const DesktopMainLayout = ({sideColumn, children}) => {
  return (
    <>
      <DesktopHeader />
      <div className={classNames([style.grid, {[style.withSideColumn]: sideColumn}])}>
        <div className={style.mainContent}>
          {children}
        </div>
        <If condition={sideColumn}>
          <div className={style.sideColumn}>
            {sideColumn}
          </div>
        </If>
      </div>
    </>
  );
};

DesktopMainLayout.propTypes = {
  sideColumn: PropTypes.node
};

