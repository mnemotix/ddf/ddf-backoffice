/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


import {ExpressApp, NetworkLayerAMQP} from '@mnemotix/synaptix.js';
import {DatastoreAdapter} from '../datastore/DatastoreAdapter';
import {dataModel} from '../datamodel/dataModel';

/**
 * A function to be passed to synaptix.js launchApplication's `generateGraphQLEndpoints` parameter
 * Initialize some configuration for the graphQL engine (datastore, network layer, graphQL schema from the data model)
 *
 * @param {ExpressApp} app - The synapix.js ExpressApp instance which will run the application server
 */
export async function generateGraphQLEndpoints({app}) {
  /**
   * Connecting network layer.
   */
  const amqpURL = `amqp://${process.env.RABBITMQ_LOGIN}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`;

  let networkLayer = new NetworkLayerAMQP(
    amqpURL,
    process.env.RABBITMQ_EXCHANGE_NAME,
    {},
    {
      durable: !!parseInt(process.env.RABBITMQ_EXCHANGE_DURABLE || 1)
    }
  );

  await networkLayer.connect();

  app.addNetworkLayer({networkLayer});

  /**
   * Initializing datastore adapter (data layer).
   */
  const datastoreAdapter = new DatastoreAdapter({
    networkLayer,
    modelDefinitionsRegister: dataModel.generateModelDefinitionsRegister()
  });

  app.addDatastoreAdapter({datastoreAdapter});

  /**
   * Initializing GraphQL endpoints
   */
  return [
    {
      endpointURI: '/graphql',
      endpointSubscriptionsURI: "/subscriptions",
      graphQLSchema: dataModel.generateExecutableSchema({
        // printGraphQLSchemaPath: path.join(__dirname, 'datamodel/schema.graphql'), /* Optionnal to print schema.graphql */
        // printJSONSchemaPath: path.join(__dirname, 'datamodel/schema.json'),        /* Optionnal to print schema.json    */
        // printJSONSchemaDotOrgPath: path.join(__dirname, '../client/schema/schema.json')        /* Optionnal to print schema.json    */
      }),
      datastoreAdapter,
      acceptAnonymousRequest: !!parseInt(process.env.OAUTH_DISABLED || 0)// Set true to bypass authentication (ONLY for test purposes)
    }
  ];
};
