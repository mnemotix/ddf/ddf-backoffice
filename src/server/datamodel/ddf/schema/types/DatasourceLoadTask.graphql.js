/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  ObjectDefaultGraphQLProperties,
  generateTypeResolvers,
  generateConnectionForType,
  generateConnectionResolverFor,
  getObjectResolver,
  getObjectsResolver,
  connectionArgs,
  filteringArgs
} from "@mnemotix/synaptix.js";
import DatasourceLoadTaskDefinition from '../../definitions/DatasourceLoadTaskDefinition';

export let DatasourceLoadTaskType = `
enum DatasourceType{
  inventaire
  wiktionnaire
}

type DatasourceLoadTask {
  """ Id of tasks """
  id: ID!
  
  """ Type of the datasource to load """
  datasourceType: DatasourceType!
  
  """ URL of the datasource to load """
  datasourceURL: String!
  
  """ Loading status """
  stageStatus: String
  
  """ Current loading stage progress """
  stageProgress: Int
  
  """ Current stage number """
  currentStage: Int
  
  """ Total stages number """
  totalStages: Int
}
  
${generateConnectionForType("DatasourceLoadTask")}
  
input DatasourceLoadTaskInput {
  """ URL of the datasource to load """
  datasourceURL: String!
  
   """ Type of the datasource to load """
  datasourceType: DatasourceType!
}

extend type Query{
  """ Search for datasource tasks """
  datasourceLoadTasks(${connectionArgs}, ${filteringArgs}): DatasourceLoadTaskConnection
  
  """ Get DatasourceLoadTask """
  datasourceLoadTask(id:ID): DatasourceLoadTask
}
`;


export let DatasourceLoadTaskTypeResolvers = {
  DatasourceLoadTask: {
    ...generateTypeResolvers("DatasourceLoadTask"),
  },
  Query: {
    datasourceLoadTasks: getObjectsResolver.bind(this, DatasourceLoadTaskDefinition),
    datasourceLoadTask: getObjectResolver.bind(this, DatasourceLoadTaskDefinition)
  },
  ...generateConnectionResolverFor("DatasourceLoadTask")
};

