/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {SynaptixDatastoreAdapter, NetworkLayerAMQP, ModelDefinitionsRegister, logDebug} from '@mnemotix/synaptix.js';
import {DatastoreSession} from "./DatastoreSession";

export class DatastoreAdapter extends SynaptixDatastoreAdapter {
  /**
   * @param {ModelDefinitionsRegister} modelDefinitionsRegister
   * @param {NetworkLayerAMQP} networkLayer
   */
  constructor({modelDefinitionsRegister, networkLayer}) {
    super({
      networkLayer,
      modelDefinitionsRegister,
      SynaptixDatastoreSessionClass: DatastoreSession
    });
  }

  /**
   * Returns a connection driver with context
   * @param {GraphQLContext} context GraphQL context object
   *
   * @returns {SynaptixDatastoreSession};
   */
  getSession(context) {
    return new (this._SynaptixDatastoreSessionClass)({
      context,
      networkLayer: this._networkLayer,
      modelDefinitionsRegister: this._modelDefinitionsRegister,
      pubSubEngine: this._pubSubEngine,
      schemaNamespaceMapping: JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING),
      nodesNamespaceURI: process.env.NODES_NAMESPACE_URI,
      nodesPrefix: process.env.NODES_PREFIX,
    });
  }

  async init() {
    //////////////////////////////////////////
    // This simulate a datasource load task //
    //////////////////////////////////////////
    // await this._networkLayer.listen("datasource.load.wiktionnaire", ({body}, fields, options) => {
    //   let stageProgress = 0;
    //   let totalStages = 10;
    //   let currentStage = 0;
    //   let datasourceURL = `${body.datasourceURL}#<${Date.now()}>`;
    //
    //   let updateInterval = setInterval(() => {
    //     stageProgress = stageProgress + 10;
    //
    //     if(stageProgress === 100){
    //       stageProgress = 0;
    //       currentStage++;
    //     }
    //
    //     if(currentStage === totalStages + 1){
    //       clearInterval(updateInterval);
    //     }
    //
    //     return this._networkLayer.publish("datasource.load.progress", {
    //       "datasourceType": "wiktionnaire",
    //       "datasourceURL": datasourceURL,
    //       "stageStatus": `Stage ${currentStage}`,
    //       "stageProgress": stageProgress,
    //       "currentStage": currentStage,
    //       "totalStages": totalStages
    //     }, {
    //       contentType: "application/json",
    //     });
    //   }, 1000);
    // });
  }
}
