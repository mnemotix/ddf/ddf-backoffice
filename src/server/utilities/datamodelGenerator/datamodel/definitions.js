/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {getFilesDirectory, HEADER_CODE, isSuperClassExposed, removeFile, writeDirectory, writeFile} from './common';


/**
 * Generate a Synaptix definition model class for modelName in parameter
 *
 * @param {namespace} namespace
 * @param {model} model
 */
const buildDefinitionCode = (namespace, model, exposeInheritanceOverNamespaces) => {

  // Check if we expose super class links (in class declaration)
  const exposeSuperClass = isSuperClassExposed(model, exposeInheritanceOverNamespaces);

  const superClassName = (exposeSuperClass)
    ? `${model.superClass.name}Definition`
    : 'ModelDefinitionAbstract';

  const result = `${HEADER_CODE}

import {ModelDefinitionAbstract, LiteralDefinition, LabelDefinition, LinkDefinition} from "@mnemotix/synaptix.js";
${(exposeSuperClass) ? `import ${superClassName} from "../${model.superClass.namespace}/${superClassName}";` : ``}
import ${model.name} from "../models/${model.name}";
${Object.values(model.links).map(link => (
    `import ${link.type.value}Definition from "./${link.type.value}Definition"`)).join(";\n")}

export default class ${model.name}Definition extends ${superClassName} {
  /**
   * Get node type as defined in a triplestore database
   * @returns {object}
   */
  static getRdfPrefixesMapping() {
    return {
        ...super.getRdfPrefixesMapping()
    };
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "${namespace}:${model.name}";
  }

  /**
   * @inheritDoc
   */
  static getModelClass() {
    return ${model.name};
  }
  
  /**
   * @inheritDoc
   */
  static getIndexType() {
    // return 'nroArea';
  }
  
  /**
   * @inheritDoc
   */
  static getIndexMatcher() {
    // return ${model.name}IndexMatcher;
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
    ${(exposeSuperClass) ? `...super.getLinks(),` : ''}
    ${Object.values(model.links).map(link => (
    `new LinkDefinition({
        linkName: '${link.name}',
        pathInIndex: '${link.name}',
        rdfObjectProperty: '${link.type.namespace}:${link.name}',
        relatedModelDefinition: ${link.type.value}Definition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false
    })`)).join(',')}
    ]
  }

  /**
   * @inheritDoc
   */
  static getLabels() {
    return [
      ${(exposeSuperClass) ?
    `     ...super.getLabels(),` : ``}
      ${Object.values(model.labels).map(label => (
    `     new LabelDefinition({
        labelName: '${label.name}',
        pathInIndex: '${label.name}s',
        rdfDataProperty: '${label.namespace}:${label.name}'
      })`)).join(',')}
    ];
  }

  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      ${Object.values(model.literals).map(literal => (
    `     new LiteralDefinition({
        literalName: '${literal.name}',
        rdfDataProperty: '${literal.namespace}:${literal.name}',
        rdfDataType: '${literal.type.prefix}${literal.type.value}'
      })`)).join(',')}
    ];
  }
};

`;

  return result;
};


/**
 * Build code for index.js file in namespace definitions directory
 *
 * @param {*} namespace
 * @param {*} definitionNames
 */
const buildDefinitionIndexCode = (namespace, definitionsNames) => {

  const result = `${HEADER_CODE}

${definitionsNames.map(definition => (
    `import ${definition} from "./${definition}.js";`
  )).join('\n')}

export {
  ${definitionsNames.join(', ')}
}

`;

  return result
};


/**
 * Build code for index.js in base definitions directory
 *
 * @param {*} namespaces
 */
const buildGlobalDefinitionIndex = (namespaces) => {

  // Upper case on first letter
  const toUpperCase = (ns) => (ns.charAt(0).toUpperCase() + ns.slice(1));

  const result = `${HEADER_CODE}

${namespaces.map(ns => (
    `import * as ${toUpperCase(ns)}ModelDefinitions from "./${ns}";`
  )).join('\n')}

export let ModelDefinitions = [].concat(
  ${namespaces.map(ns => `Object.values(${toUpperCase(ns)}ModelDefinitions)`).join(',\n\t')}
);

`;

  return result
};


/**
 * Regenerate index.js for JS modules contained in namespace definitions directory
 *
 * @param {*} baseDir
 * @param {*} namespace
 *
 */
const regenerateDefinitionsIndex = (baseDir, namespace) => {
  const namespaceDir = `${baseDir}/${namespace}/definitions`;
  writeDirectory(namespaceDir);

  // Get JS modules to index in namespace definitions
  let indexedNames = getFilesDirectory(namespaceDir);
  indexedNames = indexedNames
    .filter(fn => fn !== 'index.js')
    .filter(fn => fn.endsWith('.js'))
    .map(fn => fn.split('.js')[0]);

  writeFile(`${namespaceDir}/index.js`, buildDefinitionIndexCode(namespace, indexedNames), true);
};


/**
 * Generate model definition files corresponding to namespace and model names in argument
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {*} models
 * @param {*} force
 */
export const generateDefinitions = (baseDir, namespace, models, force = false) => {
  const definitionsDir = `${baseDir}/${namespace}/definitions`;
  writeDirectory(definitionsDir);

  // Write Synaptix definition class for each model and return the ones written
  const writtenDefinitions = Object.values(models).filter(model =>
    writeFile(`${definitionsDir}/${model.name}Definition.js`, buildDefinitionCode(namespace, model), force)
  );

  regenerateDefinitionsIndex(baseDir, namespace);

  console.log(`Definitions generated : ${writtenDefinitions.map(definition => definition.name).join(', ')}`)
};


/**
 * Remove definitions files contained in directory baseDir
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {*} models
 */
export const removeDefinitions = (baseDir, namespace, models) => {
  Object.values(models).map(model => removeFile(`${baseDir}/${namespace}/definitions/${model.name}Definition.js`));

  regenerateDefinitionsIndex(baseDir, namespace);
};