/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  getFilesDirectory,
  GRAPHQL_TYPE_CONVERTER,
  HEADER_CODE,
  isSuperClassExposed,
  removeFile,
  writeDirectory,
  writeFile
} from './common';


const lowerFirstCase = (string) => string.charAt(0).toLowerCase() + string.slice(1);

const upperFirstCase = (string) => string.charAt(0).toUpperCase() + string.slice(1);


/**
 * Generate a Synaptix schema type code for modelName in parameter
 *
 * @param {namespace} namespace
 * @param {model} model
 */
const buildSchemaTypesCode = (namespace, model, superModels = []) => {

  let literalsProperties = model.literals,
    labelsProperties = model.labels,
    linksProperties = model.links;

  // If there is super classes, we concatenate their properties whit current model properties
  superModels.map(superModel => {
    literalsProperties = {...superModel.literals, ...literalsProperties};
    labelsProperties = {...superModel.labels, ...labelsProperties};
    linksProperties = {...superModel.links, ...linksProperties};
  });

  const result = `${HEADER_CODE}

import {generateBaseResolverMap, generateConnectionResolverFor, generateConnectionForType,
    getObjectsResolver, getObjectResolver, ObjectDefaultGraphQLProperties, getLinkedObjectResolver,
    getLocalizedLabelResolver, connectionArgs, paginationArgs} from "@mnemotix/synaptix.js";
import ${model.name}Definition from '../../definitions/${model.name}Definition';


export let ${model.name}Type = \`
type ${model.name} implements ObjectInterface {
    """ The ID """
    id: ID!
    ${Object.values(literalsProperties).map(literal => (
    ` """ ${upperFirstCase(literal.labels.en)}"""
  ${literal.name}: ${GRAPHQL_TYPE_CONVERTER[literal.type.value]}
`)).join('\n\t\t')}
    ${Object.values(labelsProperties).map(label => (
    ` """ ${upperFirstCase(label.labels.en)}"""
  ${label.name}: ${GRAPHQL_TYPE_CONVERTER[label.type.value]}
`)).join('\n\t\t')}
    ${Object.values(linksProperties).map(link => (
    ` """ ${upperFirstCase(link.labels.en)}"""
  ${link.name}: ${upperFirstCase(link.type.value)}
`)).join('\n\t\t')}
    
  \${ObjectDefaultGraphQLProperties}
}
  
\${generateConnectionForType("${model.name}")}
  

input ${model.name}Input {
  """ The ID """
  id: ID @formInput(type:"hidden")
  ${Object.values(literalsProperties).map(literal => (
    ` """ ${upperFirstCase(literal.labels.en)}"""
  ${literal.name}: ${GRAPHQL_TYPE_CONVERTER[literal.type.value]}
`)).join('\n\t\t')}
  ${Object.values(labelsProperties).map(label => (
    ` """ ${upperFirstCase(label.labels.en)}"""
  ${label.name}: ${GRAPHQL_TYPE_CONVERTER[label.type.value]}
`)).join('\n\t\t')}
}


extend type Query{
  """ Search for ${model.name} """
  ${lowerFirstCase(model.name)}s(\${connectionArgs}, \${paginationArgs}): ${model.name}Connection

  """ Get ${model.name} """
  ${lowerFirstCase(model.name)}(id:ID): ${model.name}
}
\`;
  
  
export let ${model.name}ResolverMap = {
    ${model.name}:{
      ...generateBaseResolverMap("${model.name}"),
      ${Object.values(model.literals).map(literal => (
    `${literal.name}: (object) => object.${literal.name},`
  )).join('\n\t\t\t')}
      ${Object.values(model.labels).map(label => (
    `${label.name}: getLocalizedLabelResolver.bind(this, ${model.name}Definition.getLabel('${label.name}')),`
  )).join('\n\t\t\t')}
      ${Object.values(model.links).map(link => (
    `${link.name}: getLinkedObjectResolver.bind(this, ${model.name}Definition.getLink('${link.name}')),`
  )).join('\n\t\t\t')}
    },
    Query:{
      ${lowerFirstCase(model.name)}s: getObjectsResolver.bind(this, ${model.name}Definition),
      ${lowerFirstCase(model.name)}: getObjectResolver.bind(this, ${model.name}Definition)
    },
    ...generateConnectionResolverFor("${model.name}")
};

`;

  return result;
};


/**
 * Build code for index.js file in namespace schema types directory
 *
 * @param {*} namespace
 * @param {*} modelNames
 */
const buildSchemaTypesIndexCode = (namespace, modelNames) => {

  const result = `${HEADER_CODE}

import {mergeResolvers} from "@mnemotix/synaptix.js";
${modelNames.map(name => (
    `import {${name}ResolverMap, ${name}Type} from "./${name}.graphql";`
  )).join('\n')}

export let ${upperFirstCase(namespace)}Resolvers = mergeResolvers(
  ${modelNames.map(name => `${name}ResolverMap`).join(',\n\t')}
);

export let ${upperFirstCase(namespace)}Types = [
  ${modelNames.map(name => `${name}Type`).join(',\n\t')}
];

`;

  return result
};


/**
 * Build code for index.js in base schema types directory
 *
 * @param {*} namespaces
 */
const buildGlobalSchemaTypesIndex = (namespaces) => {

  // Upper case on first letter
  const toUpperCase = (ns) => (ns.charAt(0).toUpperCase() + ns.slice(1));

  const result = `${HEADER_CODE}

import {mergeResolvers} from "@mnemotix/synaptix.js";
${namespaces.map(ns => (
    `import {${upperFirstCase(ns)}Resolvers, ${upperFirstCase(ns)}Types} from "./${ns}";`
  )).join('\n')}

export let Types = [
  ${namespaces.map(ns => (`...${upperFirstCase(ns)}Types`)).join(',\n\t')}
];

export let TypesResolverMap = mergeResolvers(
  ${namespaces.map(ns => (`${upperFirstCase(ns)}Resolvers`)).join(',\n\t')}
);

`;

  return result
};


/**
 * Regenerate index.js for JS modules contained in namespace schema types directory
 *
 * @param {*} baseDir
 * @param {*} namespace
 *
 */
const regenerateSchemaTypesIndex = (baseDir, namespace) => {
  const namespaceDir = `${baseDir}/${namespace}/schema/types`;
  writeDirectory(namespaceDir);

  // Get JS modules to index in namespace schema types
  let indexedNames = getFilesDirectory(namespaceDir);
  indexedNames = indexedNames
    .filter(fn => fn !== 'index.js')
    .filter(fn => fn.endsWith('.graphql.js'))
    .map(fn => fn.split('.graphql.js')[0]);

  writeFile(`${namespaceDir}/index.js`, buildSchemaTypesIndexCode(namespace, indexedNames), true);
};


/**
 * Generate schema types files corresponding to namespace and model names in argument
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {*} models
 * @param {*} force
 */
export const generateSchemaTypes = (baseDir, namespace, models, force = false, inheritanceOverNamespaces) => {
  const typesDir = `${baseDir}/${namespace}/schema/types`;
  writeDirectory(typesDir);

  // Write Synaptix schema type file for each model and return the ones written
  const writtenTypes = Object.keys(models).filter(modelName => {
    const model = models[modelName];

    let superModels = [];

    // If we expose super class links, we parse tree of super models
    // for exposing super properties in GraphQL definition
    if (isSuperClassExposed(model, inheritanceOverNamespaces)) {
      let currentModel = model;
      while (currentModel.hasSuperClass()
      && (currentModel.superClass.name in models)) {
        let superModel = models[currentModel.superClass.name];
        superModels.push(superModel);
        currentModel = superModel;
      }
    }

    return writeFile(
      `${typesDir}/${model.name}.graphql.js`,
      buildSchemaTypesCode(namespace, model, superModels),
      force
    )
  });

  regenerateSchemaTypesIndex(baseDir, namespace);

  console.log(`Schema types generated : ${writtenTypes.map(type => type).join(', ')}`)
};


/**
 * Remove schema types files contained in directory baseDir
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {*} models
 */
export const removeSchemaTypes = (baseDir, namespace, models) => {
  Object.values(models).map(model => removeFile(`${baseDir}/${namespace}/schema/types/${model.name}.graphql.js`));

  regenerateSchemaTypesIndex(baseDir, namespace);
};