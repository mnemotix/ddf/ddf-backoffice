import path from 'path';
import fs from 'fs';
import readlineSync from "readline-sync";


/**
 * Header code for each source file
 *
 */
export const HEADER_CODE = `/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */`;


/**
 * Type syntax converter from RDF to GraphQL
 *
 */
export const GRAPHQL_TYPE_CONVERTER = {
  'string': 'String',
  'integer': 'Int',
  'float': 'Float',
  'boolean': 'Boolean',
  'dateTime': 'Float'
};


/**
 * Ask confirmation on command line for question in argument (answers 'y', 'n' or 'o')
 *
 * @param {question} question
 */
export const askConfirmation = (question) => {
  let answer = '';
  const acceptableAnswers = ['y', 'n', 'o'];
  while (acceptableAnswers.indexOf(answer) == -1) {
    answer = readlineSync.question(question);
    answer = answer.toLowerCase();
    if (acceptableAnswers.indexOf(answer) == -1) {
      console.log('Bad response, say again !');
    }
  }
  return (['y', 'o'].indexOf(answer) > -1);
};


/**
 * Return only subdirectories contained in directory in argument
 *
 * @param {dir} dir
 */
export const getModulesDirectory = (dir) =>
  fs.readdirSync(dir).filter(
    name => fs.lstatSync(path.join(dir, name)).isDirectory()
  )
;


/**
 * Return files contained in directory in argument
 * @param {*} dir
 */
export const getFilesDirectory = (dir) => fs.readdirSync(dir);


/**
 * Create directory if not already existing, and return if creation was done
 *
 * @param {directory} directory
 */
export const writeDirectory = (directory) => {
  try {
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory, {recursive: true});
      return true;
    }
  } catch (e) {
    console.log(`Error on writing directory ${directory}`);
    throw e;
  }
  return false;
};


/**
 * Write content into file incidated at path.
 * If force, we overwrite file if already existing, else we ask for if we have to
 *
 * @param {*} path
 * @param {*} content
 * @param {*} force
 */
export const writeFile = (path, content, force = false) => {
  try {
    let overwrite = true;
    // If not forcing and existing file, we ask for overwriting
    if (!force && fs.existsSync(path)) {
      overwrite = askConfirmation(`Overwrite ${path} ? (y,n) `);
    }
    if (overwrite) {
      fs.writeFileSync(path, content);
      return true;
    }
  } catch (e) {
    console.log(`Error on writing file ${path}`);
    throw e;
  }
  return false;
};


/**
 * Remove form disk file existing on path in argument
 *
 * @param {*} path
 */
export const removeFile = (path) => {
  try {
    fs.unlinkSync(path);
  } catch (e) {
  }
};


/**
 * Return if we expose inheritance super class link
 * For that, we check if model has a super class and if model namespace equals super model namespace
 * If not equals, we expose if exposeInheritance is true
 *
 * @param {model} model
 * @param {exposeInheritanceOverNamespaces} exposeInheritanceOverNamespaces
 */
export const isSuperClassExposed = (model, exposeInheritanceOverNamespaces) => (
  model.hasSuperClass()
  && (!(model.namespace !== model.superClass.namespace
    && !exposeInheritanceOverNamespaces))
);