/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {getFilesDirectory, HEADER_CODE, isSuperClassExposed, removeFile, writeDirectory, writeFile} from './common';


/**
 * Build a Synaptix model class code for modelName in parameter
 *
 * @param {model} model
 */
const buildModelCode = (namespace, model, exposeInheritanceOverNamespaces) => {

  // Check if we expose super class links (in class declaration)
  const exposeSuperClass = isSuperClassExposed(model, exposeInheritanceOverNamespaces) && model.superClass.namespace;

  const superClassName = exposeSuperClass ? model.superClass.name : 'ModelAbstract';

  const result = `${HEADER_CODE}

${(exposeSuperClass)
    ? `import ${superClassName} from "../../${model.superClass.namespace}/models/${superClassName}";`
    : `import {ModelAbstract} from "@mnemotix/synaptix.js";`}

export default class ${model.name} extends ${superClassName}{}
    
`;

  return result;
};


/**
 * Build code for index.js file in namespace models directory
 *
 * @param {*} namespace
 * @param {*} modelNames
 */
const buildModelIndexCode = (namespace, modelNames) => {
  const result = `${HEADER_CODE}

${modelNames.map(name => (
    `import ${name} from "./${name}.js";`
  )).join('\n')}


export {
    ${modelNames.join(', ')}
};

`;

  return result;
};


/**
 * Regenerate index.js for JS modules contained in namespace models directory
 *
 * @param {*} baseDir
 * @param {*} namespace
 *
 */
const regenerateModelsIndex = (baseDir, namespace) => {
  const namespaceDir = `${baseDir}/${namespace}/models`;
  writeDirectory(namespaceDir);

  // Get JS modules to index in namespace models
  let indexedNames = getFilesDirectory(namespaceDir);
  indexedNames = indexedNames
    .filter(fn => fn !== 'index.js')
    .filter(fn => fn.endsWith('.js'))
    .map(fn => fn.split('.')[0]);

  writeFile(`${namespaceDir}/index.js`, buildModelIndexCode(namespace, indexedNames), true);
};


/**
 * Generate model files corresponding to namespace and model names in argument
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {*} models
 * @param {*} force
 */
export const generateModels = (baseDir, namespace, models, force = false, inheritanceOverNs = false) => {
  const modelsDir = `${baseDir}/${namespace}/models`;
  writeDirectory(modelsDir);

  // Write Synaptix model class for each model and return the ones written
  const writtenModels = Object.values(models).filter(model =>
    writeFile(`${modelsDir}/${model.name}.js`, buildModelCode(namespace, model, inheritanceOverNs), force)
  );

  regenerateModelsIndex(baseDir, namespace);

  console.log(`Models generated : ${writtenModels.map(model => model.name).join(', ')}`)
};


/**
 * Remove model files contained in directory baseDir
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {$} models
 */
export const removeModels = (baseDir, namespace, models) => {
  Object.values(models).map(model => removeFile(`${baseDir}/${namespace}/models/${model.name}.js`));

  regenerateModelsIndex(baseDir, namespace);
};