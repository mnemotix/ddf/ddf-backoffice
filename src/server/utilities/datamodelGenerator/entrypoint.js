/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
require('util').inspect.defaultOptions.depth = null;

import {generateModels, removeModels} from './datamodel/models';
import {generateDefinitions, removeDefinitions} from './datamodel/definitions';
import {generateSchemaTypes, removeSchemaTypes} from './datamodel/types';
import {generateSchemaMutations, removeSchemaMutations} from './datamodel/mutations';
import {askConfirmation, HEADER_CODE, writeFile} from './datamodel/common'

import EnvVariables from '../../../../config/environment';
import {fetchOwlClasses} from './owlFetcher';
import {initEnvironment} from '@mnemotix/synaptix.js';

/**
 * Parse script arguments, and return them if there are correct
 *
 * @param {args} args
 */
const parseArgs = (args) => {

  if (args.length < 3) {
    throw new Error('Not enough arguments')
  }

  let namespace = args[2],
    prefix = null,
    modelName = null,
    force = false,
    remove = false;

  // Check if namespace passed in argument matches one of those described in namespace mapping
  const prefixes = JSON.parse(process.env.SCHEMA_NAMESPACE_MAPPING);
  if (!(namespace in prefixes)) {
    throw new Error(
      `Namespace "${namespace}" is not bound to an URI in namespace mapping. No data model generated !`
    );
  }

  prefix = prefixes[namespace];

  const options = args.splice(3);

  // Check if there is an argument after -c option and that it does not start by a dash
  const modelOptionIndex = options.indexOf('-c');
  if (modelOptionIndex > -1) {
    if ((options.length > modelOptionIndex + 1) && options[modelOptionIndex + 1][0] !== '-') {
      modelName = options[modelOptionIndex + 1];
    } else {
      throw new Error(`Option -c must be followed by a model name`);
    }
  }

  // Check force and remove options
  if (options.indexOf('-f') > -1) {
    force = true;
  }
  if (options.indexOf('-r') > -1) {
    remove = true;
  }

  return {prefix, namespace, modelName, force, remove};
};


/**
 * Generate Synaptix data model files in base directory in argument
 *
 * @param {baseDir} baseDir
 */
const generateDataModelFiles = (baseDir, namespace, models, force, inheritanceOverNamespaces) => {
  generateModels(baseDir, namespace, models, force, inheritanceOverNamespaces);
  generateDefinitions(baseDir, namespace, models, force, inheritanceOverNamespaces);
  generateSchemaTypes(baseDir, namespace, models, force, inheritanceOverNamespaces);
  generateSchemaMutations(baseDir, namespace, models, force);
  writeFile(`${baseDir}/${namespace}/index.js`, buildIndexCode(namespace), force)

};

/**
 * Build code for index.js file in namespace schema types directory
 *
 * @param {*} namespace
 * @param {*} modelNames
 */
const buildIndexCode = (namespace) => {

  const upperNamespace = namespace.charAt(0).toUpperCase() + namespace.slice(1);

  const result = `${HEADER_CODE}

import * as ${upperNamespace}ModelDefinitions from "./definitions";
import * as ${upperNamespace}Models from "./models";

export {${upperNamespace}Resolvers, ${upperNamespace}Types} from "./schema/types";
export {${upperNamespace}MutationsResolvers, ${upperNamespace}Mutations} from "./schema/mutations";
export {${upperNamespace}ModelDefinitions, ${upperNamespace}Models};
`;

  return result
};

/**
 * Remove all files from data model related to a namespace
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {*} models
 */
const removeDataModelFiles = (baseDir, namespace, models) => {
  removeModels(baseDir, namespace, models);
  removeDefinitions(baseDir, namespace, models);
  removeSchemaTypes(baseDir, namespace, models);
  removeSchemaMutations(baseDir, namespace, models);
  console.log(`Items removed : ${Object.values(models).map(model => model.name).join(', ')}`);
};


/**
 * Main script for generating data model JS code
 *
 */
const runCodeGen = async () => {

  initEnvironment(EnvVariables, false, false);

  let args = null;

  try {
    args = parseArgs(process.argv);
  } catch (e) {
    console.log(e);
    console.log('');
    console.log(
      `Usage: npm run generate <module> [-- [-arg] ]
            -c: generate only a specific model from this module (model name must follow this option)
            -f: force overwriting
            -r: remove files`
    );
    return
  }

  const {prefix, namespace, modelName, force, remove} = args;

  const DATAMODEL_DIR = './src/server/datamodel';

  // Fetch all models and properties from which we generate JS datamodel
  const models = await fetchOwlClasses({prefix, modelName});

  if (models.length === 0) {
    console.log(`No model was found for namespace ${namespace} and class ${modelName}. Please check your command.`);
    return;
  }

  if (remove) {
    removeDataModelFiles(DATAMODEL_DIR, namespace, models);
    return;
  }

  // Get set of namespaces of all super classes
  const inheritanceLinksNamespaces = Array.from(new Set(
    Object.values(models)
      .filter(model => model.hasSuperClass() && model.superClass.namespace !== namespace)
      .map(model => model.superClass.namespace)
  ));

  // If some super classes belong to other namespaces, we ask if we expose inheritance links in the code
  const exposeInheritanceOverNamespaces = (inheritanceLinksNamespaces.length === 0)
    ? false
    : askConfirmation(
      `Some models have inheritance links with namespaces ${inheritanceLinksNamespaces.join(', ')}.
Risk is that if models of these namespaces are not yet generated, you're getting some JS errors about missing links.
Do you want to expose them anyway ?`
    );

  generateDataModelFiles(DATAMODEL_DIR, namespace, models, force, exposeInheritanceOverNamespaces);
};


runCodeGen().then(() => {
  process.exit(0);
});


/**
 * TODO :
 *
 * - Héritage
 * -- considérer les classes intermédiaires comme des interfaces au niveau du GraphQL ?
 *
 * - Gestion des conflits : que faire ? Voir avec Mathieu
 *
 * - Gestion des liens entre objets dans les mutations : voir avec Mathieu
 *
 * - Gestion de la cardinalité des liens entre les classes
 *
 * - Voir avec Flo pour la cardinalité des relations entre classes OWL(lien plural ou non)
 *
 */