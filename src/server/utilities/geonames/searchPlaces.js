/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import got from "got/source";
import {gotCache} from "./cache/gotCache";

/**
 * Search for Geonames places.
 * Use Geonames /searchJSON API and restrict to "A" feature classes (http://www.geonames.org/export/codes.html)
 *
 * @param {string} qs - Query string
 * @param {string} [lang=fr] - Preferred language
 * @param {number} [limit=10] - Limit results size
 * @param {number} [retry=0] - Retry count
 * @param {boolean} [citiesOnly=false] - Filter on cities.
 * @return {GeonamePlace[]}
 */
export let searchPlaces = async ({qs, lang, limit, retry, citiesOnly} = {}) => {
  let filtersParams = '&featureCode=PCLI&featureCode=PCLD&featureCode=ADM1&featureCode=ADM4&featureCode=ADM4&featureCode=ADM5';

  if(citiesOnly){
    filtersParams = "&featureCode=ADM3&featureCode=ADM4&featureCode=ADM5";
  }

  let {body: results} = await got(`http://api.geonames.org/searchJSON?name_startsWith=${qs}&lang=${lang}&maxRows=${limit || 10}${filtersParams}&username=${process.env.GEONAMES_USERNAME}`, {
    json: true,
    cache: gotCache
  });

  if (results.geonames){
    return results.geonames.map((place) => {
      place.id =  `geonames:${place.geonameId}`;
      place.uri = `http://www.geonames.org/${place.geonameId}`;

      if (place.countryId){
        place.countryId =  `geonames:${place.countryId}`;
      }

      return place;
    });
  } else {
    // Sometimes, geonames throws a timeout error
    if (results.status && results.status.value === 13 && retry <= 3){
      return searchPlaces({qs, lang, limit, retry: retry + 1});
    }
  }
};