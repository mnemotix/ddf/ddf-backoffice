/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
require('util').inspect.defaultOptions.depth = null;
import got from 'got/source';
import url from 'url';
import yargs from 'yargs';
import {confirm} from 'async-prompt';
import FormData from 'form-data';
import fs from 'fs';
import os from 'os';
import ora from 'ora';
import dotenv from 'dotenv';
const snooze = ms => new Promise(resolve => setTimeout(resolve, ms));

dotenv.config();


let isRepositoryExists = async ({endpointURI, repoName}) => {
  let {body: repos} = await got(`${endpointURI}/rest/repositories`, {
    json: true
  });

  return Array.isArray(repos) && repos.find(({id}) => id === repoName);
};

let removeRepository = async ({endpointURI, repoName}) => {
  await got.delete(`${endpointURI}/rest/repositories/${repoName}`);
};

let createRepository = async ({endpointURI, repoName, configFileURI}) => {
  let {body: config} = await got(configFileURI);
  const form = new FormData();

  fs.writeFileSync(`${os.tmpdir()}/config.ttl`, config);
  form.append('config', fs.createReadStream(`${os.tmpdir()}/config.ttl`));

  await got.post(`${endpointURI}/rest/repositories`, {
    body: form
  });
};

let waitForRepositoryLoaded = async ({endpointURI, repoName, spinner, totalCount}) => {
  let {body: remaining} = await got(`${endpointURI}/rest/data/import/active/${repoName}`);

  if (parseInt(remaining) !== 0) {
    spinner.text = `Waiting data to be loaded (remaining ${remaining} files)...`;
    spinner.color = Math.ceil( remaining / totalCount) > 0.5 ? 'yellow' : 'green';
    await snooze(500);
    return waitForRepositoryLoaded({endpointURI, repoName, spinner, totalCount});
  }
};

let loadDataFileInRepository = async ({endpointURI, repoName, file, context}) => {
  return got.post(`${endpointURI}/rest/data/import/upload/${repoName}/url`, {
    json: true,
    body: {
      "data": file,
      "name": file,
      "type": "url",
      "context": context,
      "replaceGraphs": [context],
      "parserSettings": {
        "failOnUnknownDataTypes": false,
        "failOnUnknownLanguageTags": false,
        "verifyDataTypeValues": false,
        "verifyLanguageTags": false,
        "verifyRelativeURIs": false,
        "verifyURISyntax": false,
        "stopOnError": false
      }
    }
  });
};

export let loadDataFromManifestFile = async () => {
  let spinner = ora().start();
  spinner.spinner = "clock";

  let {manifestURI, endpointURI, repoName, removeRepo} = yargs
    .usage("yarn data:load [options] -m [Manifest file URI]")
    .example("yarn data:load -e http://localhost:7200 -r ddf -m https://gitlab.com/mnemotix/dicofr/raw/master/ddf-repository/manifest.json")

    .option('e', {
      alias: 'endpointURI',
      default: process.env.RDFSTORE_ROOT_EXTERNAL_URI,
      describe: 'RDF store REST endpoint',
      nargs: 1
    })
    .option('r', {
      alias: 'repoName',
      default: process.env.RDFSTORE_REPOSITORY_NAME,
      describe: 'RDF repository name',
      nargs: 1
    })
    .option('m', {
      alias: 'manifestURI',
      default: process.env.RDFSTORE_DATA_MANIFEST,
      demandOption: true,
      describe: 'Manifest file URI',
      nargs: 1
    })
    .option('R', {
      alias: 'removeRepo',
      type: "boolean",
      describe: 'Force removal of the repository if exists',
      default: false
    })
    .help('h')
    .alias('h', 'help')
    .epilog('Copyright Mnemotix 2019')
    .help()
    .argv;

  spinner.info(`Process end ${endpointURI}, repo name ${repoName}`);
  spinner.info(`Process manifest file located at ${manifestURI}`);

  let {body: manifest} = await got(manifestURI, {
    json: true
  });

  if (manifest.configFileURI) {
    let {configFileURI, data} = manifest;
    let totalCount = 0;

    configFileURI = url.resolve(manifestURI, configFileURI);

    spinner.info(`Config file : ${configFileURI}`);

    let repoExists = await isRepositoryExists({endpointURI, repoName});

    if (repoExists) {
      if (!removeRepo) {
        spinner.warn(`Caution, repository "${repoName}" exists on ${endpointURI}. If you decide not to remove it, only data flagged "clearOnLoad" in the manifest will be loaded.`);

        spinner.stop();
        removeRepo = await confirm("Do you want to remove it ? [y|N] : ");
        spinner.start();
      }
    }

    if (removeRepo) {
      spinner.start(`Removing repository ${repoName}...`);
      await removeRepository({endpointURI, repoName});
      repoExists = false;
      spinner.succeed(`Repository removed.`);
    }

    if (!repoExists) {
      spinner.start(`Creating repository ${repoName}...`);
      await createRepository({endpointURI, repoName, configFileURI});
      spinner.succeed(`Repository created.`);
    }

    for (let {name, files, context, clearOnLoad, loadIfEnvIs, loadIfEnvIsNot} of data) {
      if (repoExists && !clearOnLoad) {
        spinner.info(`Data related to "${name}" not loaded. No flag "clearOnLoad" found.`);
      } else {
        if(!!loadIfEnvIs && !loadIfEnvIs.includes(process.env.NODE_ENV)) {
          spinner.info(`Data related to "${name}" not loaded, because required NODE_ENV to be set to "${loadIfEnvIs}"`);
        } else if(!!loadIfEnvIsNot && loadIfEnvIsNot.includes(process.env.NODE_ENV)) {
          spinner.info(`Data related to "${name}" not loaded, because required NODE_ENV NOT to be set to "${loadIfEnvIsNot}"`);
        } else {
          files = files.map(file => url.resolve(manifestURI, file));
          spinner.info(`Data  related to "${name}" files : \n  - ${files.join("\n  - ")}\n`);
          for (let file of files) {
            await loadDataFileInRepository({repoName, endpointURI, context, file});
          }

          totalCount += files.length;
        }
      }
    }

    await waitForRepositoryLoaded({endpointURI, repoName, totalCount, spinner: spinner.start(`Loading data...`) });
  }

  spinner.succeed(`Data imported with success.`);
  process.exit(0);
};
