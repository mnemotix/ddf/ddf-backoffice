export const fr = {
  'LOGIN': {
    'USERNAME': "Nom d'utilisateur",
    'PASSWORD': 'Mot de passe',
    'CONNECT': 'Se connecter',
    'REGISTER': 'Créer un compte',
    'FIRST_NAME': 'Prénom',
    'LAST_NAME': 'Nom de famille',
    'VALIDATE_PASSWORD': 'Confirmation du mot de passe',
    'CREATE_ACCOUNT': 'Créer le compte',
    'CANCEL': 'Annuler',

    errors: {
      "Invalid user credentials": 'Les identifiants fournis ne sont pas valides'
    }
  },
  'FORM': {
    'TOP_POSTS': {
      'FORM_HEADER': 'Forme :',
      'ETYMOLOGY_HEADER': 'Étymologie :',
      'SEE_MORE': 'voir plus',
      'DESKTOP': {
        'FORM_HEADER': 'Discussion sur la forme',
        'ETYMOLOGY_HEADER': "Discussion sur l'étymologie",
        'SEE_MORE': 'Voir plus'
      }
    },
    'SEE_ALSO': {
      'TITLE': 'Voir aussi',
      'RELATED': 'Vocabulaire apparenté',
      'FORM_VARIATION': 'Variante graphique',
      'DERIVED': 'Mots dérivés'
    },
    'NO_RESULTS': 'Pas de résultats pour cette entrée',
    'CLOSE_FORMS_SUGGESTIONS': 'SUGGESTIONS DE RECHERCHE :',
    'DEFINITION': 'Définition',
    'DESKTOP': {
      'RESEARCHED_FORM': 'Terme recherché : ',
    }
  },
  'LEXICAL_SENSE_DETAIL': {
    'EXAMPLE_LABEL': 'Exemple',
    'ABOUT_SENSE_TITLE': 'Sens et usages',
    'BACK_TO_FORM_LINK': 'Retour à la liste des défintions',
    'BACK_TO_SENSE_LINK': 'Retour à la définition',
    'ACTIONS': {
      'LIKE': 'Je valide !',
      'REPORT': 'Je signale !'
    },
    'DESKTOP': {
      'LIKE_COUNT': '{{count}} personne a validé cette définition',
      'LIKE_COUNT_plural': '{{count}} personnes\nont validé cette définition',
      'LIKE_COUNT_zero': "Personne n'a validé cette définition",
      'SOURCE': 'Source : {{source}}',
      'TOP_POSTS': {
        'SENSE_HEADER': "Discussion sur l'usage"
      },
      'DEFINITION': 'Définition',
      'SOURCES': 'Sources',
      'GEOGRAPHICAL_DETAILS': 'Précisions géographiques'
    }
  },
  'INDEX': {
    'SEARCH_PLACEHOLDER': 'Écrivez le terme recherché ici',
    'GEOLOC_PLACEHOLDER': 'Indiquer un lieu',
    'AUTO_GEOLOC': 'Utiliser ma position actuelle',
    'FOOTER': "Comprendre et partager \n les mots avec plusieurs dictionnaires \n du français parlé partout dans le monde. \n Participer à l'enrichissement collectif",
    'SEARCH_BUTTON': "Rechercher"
  }, 
  'GEOLOC': {
    'ERRORS': {
      'PERMISSION_DENIED': "Nous n'avons pas la permission d'accéder à votre localisation",
      'UNKNOWN': "Un problème nous empêche de vous géolocaliser"

    }
  },
  'FOOTER': {
    'IMPROVE_THE_DDF': 'Enrichir le'
  },
  'MOBILE_MENU': {
    'GCU': 'Mentions légales',
    'HELP': 'Aide et documentation'
  },
  'GEOGRAPHICAL_DETAILS': {
    'NO_COUNTRY_LABEL': 'Monde',
    'TITLE': 'Précisions géographiques',
    'PAGE_CAPTION': "D'après les données collectées, ce sens est un usage notamment dans les régions suivantes : "
  },
  'SOURCES_PAGE_TITLE': 'Sources',
  'HELP':  {
    'HEADER': "Aide et documentation",
    'HOW_TO_SEARCH': "Effectuer une recherche simple",
    'SECTION_LINKS': {
      'SEARCH_AND_USER_ACCOUNT': "Recherche\net compte utilisateur",
      'UNDERSTAND_ARTICLE': "Comprendre un article\n de dictionnaire",
      'HOW_TO_CONTRIBUTE': "Contribution \n et enrichissement",
      'OFFLINE': "Faire vivre le DDF\nhors des écrans"
    },
    'DESKTOP': {
      'BACK_LINK': 'Retour au menu'
    }
  },
  'DESKTOP_HEADER': {
    'TITLE': 'Backoffice du Dictionnaire\ndes francophones',
    'SEARCH_BUTTON': "Rechercher",
    'SEARCH_PLACEHOLDER': 'Écrivez le terme recherché ici'
  },
  'DESKTOP_FOOTER': {
    'PRESENTATION': 'Présentation du projet',
    'ABOUT': 'À propos',
    'GCU': 'Mentions légales', 
    'VERSION_HISTORY': 'Historique des versions',
    'OUR_PARTNERS': 'En partenariat avec : '
  }
};
