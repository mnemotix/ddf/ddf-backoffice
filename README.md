# Dictionnaire des francophones

![npm](https://img.shields.io/npm/v/@mnemotix/synaptix.js.svg?label=Synaptix.js)


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Description](#description)
- [Commandes](#commandes)
  - [Lancement d'un environnement local](#lancement-dun-environnement-local)
  - [Installation](#installation)
  - [Démarrage de l'application en local](#d%C3%A9marrage-de-lapplication-en-local)
    - [en mode production](#en-mode-production)
    - [Variables d'environnements](#variables-denvironnements)
  - [Tests](#tests)
- [Utilisation](#utilisation)
  - [Modélisation](#mod%C3%A9lisation)
    - [Modèle de données](#mod%C3%A8le-de-donn%C3%A9es)
    - [Définition du modèle de données.](#d%C3%A9finition-du-mod%C3%A8le-de-donn%C3%A9es)
    - [Définition de la structure d'une classe du modèle de données](#d%C3%A9finition-de-la-structure-dune-classe-du-mod%C3%A8le-de-donn%C3%A9es)
    - [Définition du type GraphQL et du/des resolver(s) d'une classe du modèle de données](#d%C3%A9finition-du-type-graphql-et-dudes-resolvers-dune-classe-du-mod%C3%A8le-de-donn%C3%A9es)
    - [Tout rassembler dans la classe dataModel.js](#tout-rassembler-dans-la-classe-datamodeljs)
  - [Couche réseau de communication avec le bus AMQP](#couche-r%C3%A9seau-de-communication-avec-le-bus-amqp)
  - [Session utilisateur](#session-utilisateur)
  - [Sécurisation des resolvers](#s%C3%A9curisation-des-resolvers)
  - [Application frontend](#application-frontend)
    - [Mode développement](#mode-d%C3%A9veloppement)
    - [Mode production](#mode-production)
  - [Point d'entrée de l'application.](#point-dentr%C3%A9e-de-lapplication)
- [Tests](#tests-1)
  - [Tests d'intégration](#tests-dint%C3%A9gration)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Description

L'application du Dictionnaire est un serveur NodeJS offrant d'une part un point d'entrée public sur le HUB de données. Pour cela, il démarre :

- Un point d'accès SSO pour pouvoir obtenir un jeton d'authentification.
- Un point d'accès GraphQL pour délivrer de la donnée dans un format standardisé.
- Un point d'accès SPARQL pour accéder directement au Triple Store. (A venir)

Et d'autre part servant de serveur web minimaliste pour l'application frontend. Pour plus de détail voir le [README](./src/client/README.md) 
consacré à l'application frontend React.

## Commandes

### Lancement d'un environnement local

Avant le lancer l'application, il faut un environnement de développement fonctionnel :

Au premier déploiement, installer le système de fichiers pour les volumes permanents Docker :

```
./launcher/console install
```

Puis lancer la stack :

```
./launcher/console start -all
```

### Installation

Installer Yarn et lancer la commande : 

```
yarn install
```

Elle a pour effet d'installer toutes les dépendances.

### Démarrage de l'application en local

Lancer la commande :

```
yarn start
```

Elle démarre un serveur ExpressJS sur le port 3033 (par défaut, voir la section suivante pour le changer) avec un endpoint GraphQL.

#### en mode production

Lancer d'abord la commande de build (compile les sources frontend)

```
yarn build:prod
```

Puis lancer la commande : 

```
yarn start:prod
```

#### Variables d'environnements

Il est possible avant le démarrage d'initialiser vos propres variables d'environnements.

La liste est définie dans le fichier [./config/environment.js](./config/environment.js)

Par exemple pour changer le port d'écoute du serveur, il suffit de lancer :

```
APP_PORT=80 yarn start
```

Vous pouvez également définir une liste de variable à utiliser dans le fichier `.env` à la racine (fichier non versionné). Pour que ce fichier
soit pris en compte vous devez définir la variable `USE_DOTENV`. C'est le comportement par défaut lorsqu'on lance l'application en mode production
avec `yarn start:prod`.

**Example :**

- fichier `.env`

```
APP_PORT=80
RABBITMQ_HOST=localhost
RABBITMQ_PORT=5672
```

- lancement de l'application

```
USE_DOTENV=1 yarn start
```

### Tests

Lancer la suite de test 

```
yarn test
```

Lancer la suite de test en mode debuggage, en utilisant chrome developper tools comme client de debuggage.

```
yarn test:debug
```

## Utilisation

L'application est largement basée sur la bibliothèque [Synaptix.js](https://gitlab.com/mnemotix/synaptix.js). La partie complémentaire de la documentation est disponible dans son fichier [README](https://gitlab.com/mnemotix/synaptix.js/blob/master/README.md).

### Modélisation

L'application utilise le mécanisme de description et de résolution du [modèle de données de Synaptix.js](https://gitlab.com/mnemotix/synaptix.js/blob/master/README.md#ontologies).

Les ontologies génériques suivantes sont déjà implémentées.

 - FOAF : Décrivant les acteurs et les relations entre les acteurs de l'écosystème.
   - Extension UserAccount : Qui intègre les métadonnées Keycloak dans une foaf:Person.

#### Modèle de données

L'application expose une [instance de la classe Datamodel](./src/datamodel/dataModel.js) où est décrit tout le modèle de données 
et la façon d'accèder aux données.

L'utilisation détaillée de cette classe est détaillée [ici](https://gitlab.com/mnemotix/synaptix.js#datamodel)

#### Définition du modèle de données.

Toutes les classes décrivant le modèle de données métier du Dictionnaire se trouvent dans le dossier `datamodel`.

```
/datamodel
  /[namespace] => Nom de domaine de l'ontologie (Exemple : ddf, lexicog, ontolex...)
    /definitions => Définitions fonctionnelles de l'ontologie.
    /matchers    => Définitions fonctionnelles des indexs .
    /models      => Classes JS utilitaires affiliées à chaque classe de l'ontologie
    /schema      => Définitions du schéma GraphQL
      /mutations      => Entrypoints GraphQL concernant les écritures dans la base.
      /subscriptions  => Entrypoints GraphQL concernant les souscriptions Websockets.
      /types          => Entrypoints GraphQL concernant les lectures dans la base.
    
  dataModel.js => Fichier de description général rassemblant tout ce qui précède.
```

A chaque classe de l'ontologie correspond un ensemble de classes JS étendant :
 
- Une classe décrivant la structure RDF [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js)
- Une classe utilitaire de modèle [ModelAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/models/ModelAbstract.js)
- Une classe décrivant l'index ES lié s'il existe [DefaultIndexMatcher](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/src/server/datamodel/ontologies/matchers/DefaultIndexMatcher.js)
- Une description de son Type au sens GraphQL et une définition de son/ses resolvers.

#### Définition de la structure d'une classe du modèle de données

Chaque classe de l'ontologie est définie par un [ModelDefinitionAbstract](https://gitlab.com/mnemotix/synaptix.js/blob/master/src/server/datamodel/ontologies/definitions/ModelDefinitionAbstract.js)

Exemple : Le type RDF `ontolex:LexicalEntry` est décrit dans la classe [LexicalEntryDefinition](./src/datamodel/ontolex/definitions/LexicalEntryDefinition.js)

Cette classe hérite différentes méthodes pour :

- `LexicalEntryDefinition::getRdfType()` qui retourne le type RDF à savoir `ontolex:LexicalEntry`

```js
  static getRdfType(){
    return "ontolex:LexicalEntry";
  }
```

- `LexicalEntryDefinition::getModelClass()` qui retourne la classe utilitaire JS à savoir [LexicalEntry](./src/datamodel/ontolex/models/LexicalEntry.js)

```js
  static getModelClass(){
      return LexicalEntry;
    }
```

- `LexicalEntryDefinition::getIndexType()` qui retourne le nom de l'index ES utilisé pour indexer cette classe (s'il existe).

```js
  static getIndexType(){
    return 'LexicalEntry';
  }
```

- `LexicalEntryDefinition::getIndexMatcher()` qui retourne la classe utilitaire de requêtage sur l'index.

```js
  static getIndexMatcher(){
    return LexicalEntryIndexMatcher
  }
```

- `LexicalEntryDefinition::getLinks()` qui retourne la liste des liens aux autres définitions de l'ontologie et les méthode pour y accéder (via l'Index ou via le TripleStore).

```js
  static getLinks(){
      return [
        new LinkDefinition({
          linkName: 'finalUserSites', // Nom utilitaire du lien à utiliser dans les méthodes helpers des resolvers
          pathInIndex: 'finalUserSites', // Chemin dans l'index LexicalEntry
          rdfObjectProperty: "hddgis:concernedFinalUserSite", // Predicat RDF. Exemple de triplet : ?LexicalEntryUri hddgis:concernedFinalUserSite ?finalUserSiteUri
          relatedModelDefinition: FinalUserSiteDefinition,
          isPlural: true  // => Doit retourner une liste
        }),
      ];
    }
```

- `LexicalEntryDefinition::getLabels()` qui retourne la liste des litéraux internationalisés.

```js
  static getLabels(){
      return []; // => Pas d'internationalisation dans le HUB.
    }
```

- `LexicalEntryDefinition::getLiterals()` qui retourne la liste des litéraux non internationalisés.

```js
/**
   * @inheritDoc
   */
  static getLiterals(){
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "status", // Nom utilitaire du lien à utiliser dans les méthodes helpers des resolvers
        pathInIndex: "status", // Chemin dans l'index LexicalEntry
        rdfDataProperty: "hddgis:status", // Predicat RDF : Exemple de triplet : ?LexicalEntryUri hddgis:status ?status
      	rdfDataType: "rdfs:Literal" // URI du type si besoin. Exemple pour un booléen http://www.w3.org/2001/XMLSchema#boolean
      })
    ];
  }
```

#### Définition du type GraphQL et du/des resolver(s) d'une classe du modèle de données

Pour chaque type RDF, on définit le schéma au format GraphQL correspondant et les resolvers afférants.

Exemple pour le type `ontolex:LexicalEntry` [LexicalEntry.graphql.js](./src/datamodel/ontolex/schema/types/LexicalEntry.graphql.js)

Ce fichier export deux objets :

La définition GraphQL du type `ontolex:LexicalEntry` :

```js
export let LexicalEntryType = `
type LexicalEntry implements ObjectInterface {
  """ The ID """
  id: ID!
  
  """ The lexical entry status """
  status: String
  
  [...]
  
  # Helper pour rajouter des propriétés communes à tous les types
  # (creationDate, lastUpdate, _enabled...)
  ${ObjectDefaultGraphQLProperties}
}

# Helper pour créer un type LexicalEntryConnection pour faire des liens avec d'autres objets du graphe.
${generateConnectionForType("LexicalEntry")}

# Extension du type GraphQL "Query" permettant de rajouter deux endpoints au niveau racine des requêtes GraphQL (query {...})
extend type Query{
  """ Search for lexical entries """
  lexicalEntrys(${connectionArgs}, ${filteringArgs}): LexicalEntryConnection
  
  """ Get lexical entry """
  lexicalEntry(id:ID): LexicalEntry
}
`;
```

Ainsi que la définition des resolvers pour résoudre les données :

```js
export let LexicalEntryResolverMap = {
  LexicalEntry:{
    // Helpers pour résoudre les propriétés communes à tous les types
    // (creationDate, lastUpdate, _enabled...)
    ...generateBaseResolverMap("LexicalEntry"),
    // Resolver de la propriété "status"
    status: (lexicalEntry) => lexicalEntry.status, // LexicalEntry est une instance de la classe LexicalEntry (./src/datamodel/models/gis/LexicalEntry.js)
		//...
  },
  Query:{
    // Helper pour résoudre un objet en particulier.
    lexicalEntry: getObjectResolver.bind(this, LexicalEntryDefinition)
    //...
  },
  // Helper pour résoudre les propriété commune d'une Connection
  ...generateConnectionResolverFor("LexicalEntry")
};
```

#### Tout rassembler dans la classe dataModel.js

Une fois toutes les définitions, matchers, classes modèle et définitions GraphQL définies, il reste à tout
rassembler dans une instance de la classe [Datamodel](./src/datamodel/dataModel.js)

```js
export let datamodel = new Datamodel({
  typeDefs: [].concat(                // Lister ici tous les types GraphQL (aka: Types/Mutations/Subscriptions in GraphQL format)
    Ontologies.foaf.schema.Types,
    Ontologies.foaf.schema.Mutations,
    Types,
    Mutations,
  ),
  resolvers: mergeResolvers(
    Ontologies.foaf.resolvers.Types, // Lister ici tous les objets resolvers.
    Ontologies.foaf.resolvers.Mutations,
    TypesResolverMap,
    MutationsResolverMap,
  ),
  modelDefinitions: [].concat(       // Lister ici tous les classes définition de modèle.
    Object.values(Ontologies.foaf.ModelDefinitions),
    ModelDefinitions
  )
});
```

Cette instance dispose d'une méthode `datamodel::generateExecutableSchema()` qui retourne un schéma GraphQL exécutable à 
donner au serveur GraphQL. 

Exemple dans l'[entrypoint](./src/server/middlewares/generateGraphQLEndpoints.js#L65) 

### Couche réseau de communication avec le bus AMQP

L'application utilise le mécanisme mis à disposition par [Synaptix.js](https://gitlab.com/mnemotix/synaptix.js/blob/master/README.md#amqp).

Un exemple intéressant de l'utilisation de cette API est disponible dans l'implémentation du [mécanisme de souscriptions aux alertes IoT](./src/datastore/DatastoreSession.js#L29)

### Session utilisateur

L'application est basée sur un [binôme de classes détaillé dans Synaptix.js](https://gitlab.com/mnemotix/synaptix.js/blob/master/README.md#datastore-adapters) pour gérer les sessions utilisateurs.

 - Une classe [DatastoreAdapter](./src/datastore/DatastoreAdapter.js) instanciée **une fois** au démarrage de l'application. 
 Elle est chargée d'initier tous les mécanismes utilisés pour communiquer avec la couche données.
 - Une classe [DatastoreSession](./src/datastore/DatastoreSession.js) instanciée **à chaque requête utilisateur**. Elle met à disposition des méthodes 
 pour accéder d'une part aux informations liées à la session utilisateur, et d'autre part aux données générale du Dictionnaire.
 
L'instance de cette dernière classe est accessible dans le contexte de chaque Resolver ou Souscription GraphQL.

```js
export let MyType = `
type MyType{
	[...]
}

extend type Query{
  """ Get myType """
  myType([arguments]): MyType
}
`;

export let MyTypeResolverMap = {
  Query:{
    /**
			* This is the resolver of the type "myType" available on 
			* @param {object} queryRoot - Root object resolved in root query.
			* @param {object} queryArgs - Arguments defined in myType definition
			* @param {DatastoreSession} datastoreSession - Instance on DatastoreSession
			*/
    myType: async (queryRoot, queryArgs, datastoreSession) => {
			// Resolve query here.
    }
  }
};
```

### Sécurisation des resolvers

Nous allons utiliser la bibliothèque [GraphQL Shield](https://github.com/maticzav/graphql-shield) pour sécuriser les resolvers avec des middlewares.


### Application frontend

L'application frontend est une Single Page Application, qui fonctionne avec le framework React. Les sources frontend (javascript et assets) sont
compilées & bundlées par Webpack. Webpack est configuré via un fichier unique [`webpack.config.js`](./webpack.config.js), pour le mode développement
et production. Le middleware [`serveFrontend`](./src/server/middlewares/serveFrontend.js) est responsable de servir le frontend.

#### Mode développement

Le serveur NodeJS configure et lance webpack-dev-server pour bundler en live et servir l'application frontend. 

#### Mode production

En mode production, il faut préalablement compiler/bundler les sources frontend avec webpack, ceci est fait en exécutant le script 

```
yarn build:prod
```

Les sources compilées se trouvent dans le dossier `/dist` (non versionné).

Le serveur nodeJS lancé en mode production (`yarn start:prod`) sert statiquement les fichiers du dossier `/dist`, et pour toute route 
non utilisée, sert le fichier `index.html` par défaut (ce qui permet la navigation en mode Single Page Application).

### Point d'entrée de l'application.

Le fichier [entrypoint.js](./src/server/entrypoint.js) est le point d'entrée de l'application. 

Son rôle est de :

- Initialiser la [couche réseau](./src/server/middlewares/generateGraphQLEndpoints.js#34).
- Initialiser la [couche données](./src/server/middlewares/generateGraphQLEndpoints.js#L52).
- Décrire les [points d'accès GraphQL](./src/server/middlewares/generateGraphQLEndpoints.js#L62)
- De servir l'[application web](./src/server/middlewares/serveFrontend.js), via le serveur de développement webpack (en mode développement), 
  ou en servant directement les souces compilées dans `/dist` en mode production (compilation préalablem

## Tests

La suite de tests fonctionne avec Jest. Jest est configuré via le fichier [`jest.config.js`](./jest.config.js), qui lui même référence 
des fichiers de configuration spécifiques situé dans le répertoire `/jest`. Parmi ces fichies on distingue : 

- `./jest/setup.js` et les scripts situé dans `./jest/setup/`, qui s'exécutent avant chaque test. Ils sont responsable de mettre en place l'environnement
  dans lequel vont s'exécuter les tests (extensions du framework jest, configuration de mocks, ...)
- `./jest/babelRootModeUpwardTransform` qui est un wrapper permettant de configurer babel, utilisé par Jest pour transpiler les sources (tests et code applicatif).

Le framework de test est le même pour le code client et serveur. Les tests sont exécutés dans un environnement NodeJS, avec JSDOM comme émulateur de navigateur.

### Tests d'intégration

À réaliser. Pour celà il faudrait créer une stack de test qui comprendrait : 

- un script de launch paramétré pour lancer les containers spécifique aux tests à la place de la stack de production
- créers des containers dedié au tests, pour les éléments de la stack comprenant de la persistance (graphDB, elasticsearch, ...), avec
  un jeu de donnée préchargé
